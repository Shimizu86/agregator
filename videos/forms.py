# coding=utf-8
from __future__ import unicode_literals
from django import forms

from videos.models import UserTask


class UserTaskForm(forms.ModelForm):
    class Meta:
        model = UserTask
        fields = ('site_url', 'site_tags', )
        widgets = {
            'site_url': forms.TextInput(attrs={'class': 'form-control'}),
            'site_tags': forms.TextInput(attrs={'class': 'form-control'})
        }
