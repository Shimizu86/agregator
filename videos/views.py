# coding=utf-8
from __future__ import unicode_literals
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from django.utils.decorators import method_decorator
from django.views.generic import ListView, FormView

from videos.forms import UserTaskForm
from videos.models import UserTask, Videos, ParsedUrl
from videos.tasks import make_content


class BaseMixin(object):
    js = []
    css = []
    title = ''

    def get_context_data(self, **kwargs):
        context = super(BaseMixin, self).get_context_data(**kwargs)
        context['additional_scripts'] = self.js
        context['additional_css'] = self.css
        context['title'] = self.title

        return context

class AddUserTaskView(BaseMixin, FormView):
    model = UserTask
    template_name = 'add_video.html'
    form_class = UserTaskForm
    title = 'Добавьте ссылку для видео'

    def form_valid(self, form):
        tasks = [task.site_url for task in UserTask.objects.filter(owner=self.request.user)]
        if form.cleaned_data['site_url'] not in tasks:
            form.instance.owner = self.request.user
            form.save()
            make_content.delay(form.cleaned_data['site_url'],
                               form.cleaned_data['site_tags'])
            return JsonResponse({'status': 'ok', })

        return JsonResponse({'status': 'already_exist', })

    def form_invalid(self, form):
        return JsonResponse({'status': 'error', 'errors': form.errors})

class ShowUrlVideosView(BaseMixin, ListView):
    model = ParsedUrl
    template_name = 'detail.html'
    title = 'Список ваших УРЛов'

    def get_context_data(self, **kwargs):
        context = super(ShowUrlVideosView, self).get_context_data(**kwargs)
        context['user_urls'] = UserTask.objects.filter(owner=self.request.user)
        return context

class ShowVideosView(BaseMixin, ListView):
    model = Videos
    template_name = 'detail_videos.html'
    title = 'Список ваших Видео'
