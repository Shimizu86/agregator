# coding=utf-8
from __future__ import unicode_literals
from django.contrib.auth.models import User
from django.db import models


class UserTask(models.Model):
    owner = models.ForeignKey(User)
    site_url = models.URLField(max_length=300)
    site_tags = models.CharField(max_length=100)

    def __unicode__(self):
        return self.site_url


class ParsedUrl(models.Model):
    url = models.ForeignKey(UserTask)
    urls = models.CharField(max_length=400)
    is_created_video = models.BooleanField(default=False)
    created_date = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.urls


class Videos(models.Model):
    owner = models.ForeignKey(User)
    video = models.FileField(upload_to='upload/video/%Y_%m_%d/')

    def __unicode__(self):
        return str(self.owner)
