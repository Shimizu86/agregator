# coding=utf-8
from __future__ import unicode_literals
import os

import urllib2

from BeautifulSoup import BeautifulSoup
from django.core.exceptions import ValidationError
from django.core.validators import URLValidator

from agregator.celery import app
from videos.models import ParsedUrl, UserTask


@app.task
def make_content(site_url, site_tags):

    user_task = UserTask.objects.get(site_url=site_url)
    page = urllib2.urlopen(site_url)
    soup = BeautifulSoup(page)
    links = soup.findAll(site_tags, href=True)
    href_validator = URLValidator()
    for url in links:
        href = url['href']
        try:
            href_validator(href)
        except ValidationError:
            continue
        parsed_url = ParsedUrl(url=user_task, urls=href)
        parsed_url.save()



#
# from moviepy.editor import VideoFileClip, concatenate_videoclips
#
# @app.task
# def do_concat():
#     intro = VideoFileClip(os.path.join(MEDIA_ROOT, 'intro.webm'))
#     recording = VideoFileClip(os.path.join(MEDIA_ROOT, 'recording.webm'))
#     outro = VideoFileClip(os.path.join(MEDIA_ROOT, 'outro.webm'))
#     final_clip = concatenate_videoclips([intro, recording, outro])
#     final_clip.write_videofile(os.path.join(MEDIA_ROOT, 'new_clip.webm'), fps=24)