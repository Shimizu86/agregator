# coding=utf-8
from __future__ import unicode_literals
from django.contrib import admin

from .models import UserTask, ParsedUrl, Videos


class ParsedUrlAdmin(admin.ModelAdmin):
    fields = ('url', 'urls', 'is_created_video')


admin.site.register(UserTask)
admin.site.register(ParsedUrl, ParsedUrlAdmin)
admin.site.register(Videos)
