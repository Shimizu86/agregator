# coding=utf-8
from __future__ import unicode_literals
from django.conf.urls import url

from .views import ShowUrlVideosView, ShowVideosView, AddUserTaskView

urlpatterns = [
    url(r'^add_task/', AddUserTaskView.as_view(), name='add_user_task'),
    url(r'^show_task/', ShowUrlVideosView.as_view(), name='show_user_task'),
    url(r'^show_result', ShowVideosView.as_view(), name='show_result'),
]
