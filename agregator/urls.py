from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^video/', include('videos.urls')),
    url(r'^accounts/', include('registration.backends.default.urls')),
]
