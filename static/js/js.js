$(document).ready(function(){
    var content = $('#add-url-form'),
        success_text = $("#success_text"),
        already_exists_text = $("#already_exists_text");
    success_text.hide();
    already_exists_text.hide();
    content.submit(function(e){
        e.preventDefault();
        $.ajax({
            url: $('#add-url-form'),
            type: 'post',
            data: content.serialize(),
            success : function(data){
                $(".form-group").addClass("has-success").removeClass("has-error");
                $(".help-inline").hide();
                success_text.hide();
                if (data.status == 'error'){
                    for (error in data.errors){
                        $("#id_" + error).addClass("has-error").removeClass("has-success");
                        $("#id_" + error + "_error").html(data.errors[error]).show();
                    }
                }
                if (data.status == 'ok'){
                    success_text.show();
                    already_exists_text.hide();
                }
                else if (data.status =='already_exist'){
                    already_exists_text.show();
                    success_text.hide();
                }
            }
        })
    });
});